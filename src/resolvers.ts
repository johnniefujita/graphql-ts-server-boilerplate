
import {ResolverMap} from "./types/graphql-utils"
import * as bcrypt from 'bcryptjs';
import { User } from "./entity/User";

export const resolvers: ResolverMap = {
    Query: {
        hello: (_: any, { name }: GQL.IHelloOnQueryArguments) => `Hello ${name || 'World!'}`,
    },
    Mutation: {
        register: async (_, {firstName, lastName, email, password}: GQL.IRegisterOnMutationArguments ) => {
            const hashedPsswd = await bcrypt.hash(password, 10);

            const user = User.create({
                firstName,
                lastName,
                email,
                password: hashedPsswd
            });
            await user.save()

            return true
        }
    }
}
