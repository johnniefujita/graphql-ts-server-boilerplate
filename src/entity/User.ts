import {Entity,BaseEntity, PrimaryColumn, Column, BeforeInsert} from "typeorm";
import * as  uuidv4 from 'uuid/v4';
@Entity("_user")
export class User extends BaseEntity {

    @PrimaryColumn("uuid")
    id: string;

    @Column("varchar", { length: 60})
    firstName: string;

    @Column("varchar", { length: 120})
    lastName: string;

    @Column("varchar", {length: 255})
    email: string;

    @Column("text")
    password: string;

    @BeforeInsert()
    addId() {
        this.id = uuidv4()
    }
}
