import "reflect-metadata";
import { GraphQLServer } from 'graphql-yoga';
// import {User} from "./entity/User";

import { resolvers } from "./resolvers";

import * as path from 'path'

import { importSchema } from 'graphql-import';
import { makeExecutableSchema } from 'graphql-tools';
import { createTypeOrmConn } from "./utils/createTypeOrmConn";
 


export const startServer = async () => {
  const typeDefs = importSchema(path.join(__dirname, './schema.graphql'));

 
  const schema = makeExecutableSchema({ typeDefs, resolvers })

 

  const server = new GraphQLServer({ schema });
  await createTypeOrmConn();
  await server.start();
  console.log("Graphql Server, running on localhost:4000");
  
  };

startServer();

