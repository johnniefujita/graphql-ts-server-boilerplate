//import { startServer } from "..";
import { request } from "graphql-request";
import { host } from "./constants";

import { User } from "../entity/User";
import { createTypeOrmConn } from "../utils/createTypeOrmConn";


beforeAll(async () =>{
   await createTypeOrmConn();
  
    
})

const email = 'johnnieadsadklpçdfsdfs.johnine@gmail.com';
const password = "kdskald";

const mutation = `
    mutation {
        register(firstName: "johnnie", lastName: "Fimose", password: "${password}", email: "${email}")
    }
`

test("Register user", async () => {
   
    const response = await request(host, mutation);
    expect(response).toEqual({register: true});
    
    const users = await User.find({ where: { email }});
    expect(users).toHaveLength(1);
    const user = users[0];
    expect(user.email).toEqual(email);
    expect(user.password).not.toEqual(password);
});
